<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DownloadController extends Controller
{
    public function index()
    {
    	$downloads = DB::table('downloads')->get();

    	return view('download', ['downloads' => $downloads]);
    }

    public function get($id)
    {
    	$unduh = DB::table('downloads')->where(['id' => $id])->first();

		return response()->download(storage_path('app/public/'.$unduh->path));
    }
}
