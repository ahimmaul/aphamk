<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class JournalController extends Controller
{
    public function index()
    {
    	$journals = DB::table('journals')->get();

    	return view('journal', ['journals' => $journals]);
    }

    public function get($id)
    {
    	$journal = DB::table('journals')->where(['id' => $id])->first();
    	$headers = [
              'Content-Type' => 'application/pdf',
           ];

        return view('journal-read', compact('journal'));
		return response()->download(storage_path('app/downloads/'.$journal->path, $headers));
    }
}
