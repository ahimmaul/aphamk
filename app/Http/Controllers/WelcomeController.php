<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class WelcomeController extends Controller
{
    public function index()
    {
    	$downloads = DB::table('downloads')->limit(4)->get();
    	$slides = DB::table('albums')->where('is_slide', 1)->get();

    	return view('welcome', ['downloads' => $downloads, 'slides' => $slides]);
    }
}
