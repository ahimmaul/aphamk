@extends('layouts.welcome')

@section('page_title', 'Unduhan')

@section('content')
<div class="container">
    <div class="row" style="margin-top: 20px">
        <div class="card bg-default mb-3" style="">
          <div class="card-header">
            <strong>Unduhan</strong>
          </div>
          <ul class="list-group list-group-flush text-dark">
            @foreach($downloads as $download)
            <li class="list-group-item"><a href="{{ url('unduh/'.$download->id) }}">{{ $download->name }}</a></li>
            @endforeach
          </ul>
        </div>
    </div>
</div>
@endsection