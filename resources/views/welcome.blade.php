@extends('layouts.welcome')

@section('page_title', 'Beranda')

@section('content')
<div class="container">
    <div class="row" style="margin-top: 20px">
        <div class="col-lg-4">
            <div class="card bg-default mb-3" style="">
              <div class="card-header">
                <strong>Unduhan</strong>
              </div>
              <ul class="list-group list-group-flush text-dark">
                @foreach($downloads as $download)
                <li class="list-group-item"><a href="{{ url('unduh/'.$download->id) }}">{{ $download->name }}</a></li>
                @endforeach
                <li class="list-group-item"><a href="{{ url('unduh') }}" class="txt-default">Selengkapnya</a></li>
              </ul>
            </div>
        </div>
        <div class="col-lg-8">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach($slides as $slide)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" @if($loop->first) class="active" @endif></li>
                    @endforeach
                </ol>
                <div class="carousel-inner" style="height: 300px">
                    @foreach($slides as $slide)
                    <div class="carousel-item @if($loop->first) active @endif slider-size">
                        <img class="d-block w-100" src="{{ url('img/'.$slide->name) }}" style="height: 100%">
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<style type="text/css">
    /* Mobile */
    @media (max-width: 767px) {
        .slider-size {
            height: auto;
        }
        .slider-size > img {
             width: 80%;
        }
    }

    /* tablets */
    @media (max-width: 991px) and (min-width: 768px) {
        .slider-size {
            height: auto;
        }
        .slider-size > img {
            width: 80%;
        }
    }

    /* laptops */
    @media (max-width: 1023px) and (min-width: 992px) {
        .slider-size {
             height: 200px;
        }
        .slider-size > img {
            width: 80%;
        }
    }

    /* desktops */
    @media (min-width: 1024px) {
        .slider-size {
            height: 300px;
        }
        .slider-size > img {
            width: 60%;
        }
    }
</style>
@endsection

@section('script')
<script type="text/javascript">
    $('.carousel').carousel({
      interval: 2000
    })
</script>
@endsection