<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href="{{ url('img/favicon.png') }}"/>
        <title>@yield('page_title') - APHAMK</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <style type="text/css">
            .footer {
              width: 100%;
              /* Set the fixed height of the footer here */
              height: 60px;
              line-height: 60px; /* Vertically center the text there */
              margin-top: 20px;
            }

            .bg-default {
                background-color: #cc3333;
                color: #fff;
            }

            .txt-default, .txt-default:hover {
                color: #cc3333;
            }

            @media (min-width: 768px) {
                .navbar-nav {
                    float:none;
                    margin:0 auto;
                    display: block;
                    text-align: center;
                }

                .navbar-nav > li {
                    display: inline-block;
                    float:none;
                }
            }

            .modal-dialog {
                max-width: 800px;
                margin: 30px auto;
            }

            .modal-body {
                position: relative;
                padding: 0px;
            }

            .close {
                position: absolute;
                right: -30px;
                top: 0;
                z-index: 999;
                font-size: 2rem;
                font-weight: normal;
                color: #fff;
                opacity: 1;
            }
        </style>
        @yield('style')
    </head>
    <body>
        <div id="header">
            <div class="col-lg-12 col-md-12" align="center">
                <img src="{{ url('img/logo.png') }}" style="height: 150px; margin-top: 10px; margin-bottom: 10px" class="img-fluid">
            </div>
        </div>

        <nav class="navbar navbar-expand-md navbar-dark bg-default" style="">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>

            <div class="container">
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item{{ request()->is('/') ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('/') }}">BERANDA</a>
                        </li>
                        <li class="nav-item{{ request()->is('pengurus-anggota') ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('pengurus-anggota') }}">PENGURUS & ANGGOTA</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link video-btn" href="#" data-toggle="modal" data-target="#myModal" data-src="{{ env('VIDEO_LINK') }}">VIDEO MARS</a>
                        </li>
                        <li class="nav-item{{ request()->is('unduh') ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('unduh') }}">UNDUHAN</a>
                        </li>
                        <li class="nav-item{{ request()->is('jurnal/*') || request()->is('jurnal')  ? ' active' : '' }}">
                            <a class="nav-link maintenance" href="#">JURNAL</a>
                        </li>
                        <li class="nav-item{{ request()->is('gallery') ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('gallery') }}">GALERI</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
        
        
                    <div class="modal-body">
        
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <!-- 16:9 aspect ratio -->
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always"
                                allow="autoplay" allowfullscreen></iframe>
                        </div>
        
        
                    </div>
        
                </div>
            </div>
        </div>

        @yield('content')

            <footer class="footer bg-default">
              <div class="container">
                © Copyright 2019 APHAMK.
              </div>
            </footer>

        {{-- <audio id="myAudio" autoplay loop>
          <source src="{{ url('2017-07-27-AUDIO-00000119.mp3') }}" type="audio/mpeg">
          Your browser does not support the audio element.
        </audio> --}}

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-58049176-10"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-58049176-10');

          $(document).ready(function() {
            $('.maintenance').click(function() {
                alert('Fitur sedang maintenance');
            });

            var $videoSrc;
            $('.video-btn').click(function () {
                $videoSrc = $(this).data("src");
                console.log($videoSrc);
            });

            $('#myModal').on('shown.bs.modal', function (e) {
                $("#video").attr('src', $videoSrc + "?cc_load_policy=1&autoplay=1&amp;modestbranding=1&amp;showinfo=0");
            })

            $('#myModal').on('hide.bs.modal', function (e) {
                $("#video").attr('src', $videoSrc);
            })
          });
        </script>

        @yield('script')
    </body>

</html>
