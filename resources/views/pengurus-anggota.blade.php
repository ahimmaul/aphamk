@extends('layouts.welcome')

@section('page_title', 'Pengurus & Anggota')

@section('content')
<div class="container">
    <div class="row" style="margin-top: 20px">
        <div class="card bg-default mb-3" style="">
          <div class="card-header">
            <strong>Daftar Pengurus</strong>
          </div>
          <div class="card-body">
            <ul>
              <li>Penasihat : Prof. Dr. Moh. Mahfud MD, S.H.</li>
              <li>Pembina : Hakim-Hakim Mahkamah Konstitusi (sebagai Pribadi)</li>
              <li>Pengawas  : Ardilafiza, S.H.,M.Hum.</li>
            </ul>
            <h5>Dewan Pimpinan Pusat (DPP)</h5>
            <ul>
              <li>Ketua Umum  : Dr. Widodo Ekatjahjana, S.H.,M.Hum.</li>
              <li>Wakil Ketua : Dr. Saifudin, S.H.,M.Hum.</li>
              <li>Sekretaris  : Sunny Ummul Firdaus, S.H.,M.H.</li>
              <li>Wakil Sekretaris  : Septi Nurwijayanti, S.H.,M.Hum.</li>
              <li>Bendahara : Mohammad Novrizal Bahar, S.H.,LLM.</li>
              <li>Wakil Bendahara : Fifiana Wishaeni, S.H.,M.Hum.</li>
            </ul>
            <h5>Daftar Ketua DPD APHAMK</h5>
            <table class="table">
              <tr>
                <th>No.</th>
                <th>DPD</th>
                <th>Nama Ketua</th>
                <th>Asal Universitas</th>
              </tr>
              <tr>
                <td>1</td>
                <td>Jawa Timur</td>
                <td>Siti Marwiyah</td>
                <td>Univ. Dr. Soetomo Surabaya</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jawa Barat</td>
                <td>Rusli Iskandar</td>
                <td>Univ. Islam Bandung</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Bali</td>
                <td>I Gede Yusa</td>
                <td>Univ. Udayana Bali</td>
              </tr>
              <tr>
                <td>4</td>
                <td>Maluku dan Papua</td>
                <td>Jantje Tjiptabudy</td>
                <td>Univ. Pattimura</td>
              </tr>
              <tr>
                <td>5</td>
                <td>Lampung</td>
                <td>Muhtadi</td>
                <td>Univ. Lampung</td>
              </tr>
              <tr>
                <td>6</td>
                <td>Sulawesi Selatan</td>
                <td>Audi H. Pondaag</td>
                <td>Univ. Sam Ratulangi</td>
              </tr>
              <tr>
                <td>7</td>
                <td>Kalimantan Selatan</td>
                <td>Ichsan Anwary</td>
                <td>Univ. Lambung Mangkurat</td>
              </tr>
              <tr>
                <td>8</td>
                <td>DIY</td>
                <td>Anang Zubaidy</td>
                <td>Univ. Islam Indonesia Yogyakarta</td>
              </tr>
              <tr>
                <td>9</td>
                <td>Aceh</td>
                <td>Zahratul Idami</td>
                <td>Univ. Syiah Kuala</td>
              </tr>
              <tr>
                <td>10</td>
                <td>Jambi</td>
                <td>Mery Yarni</td>
                <td>Univ. Jambi</td>
              </tr>
              <tr>
                <td>11</td>
                <td>Maluku Utara</td>
                <td>Muhammad Asykin</td>
                <td>Univ. Khairun</td>
              </tr>
              <tr>
                <td>12</td>
                <td>Jakarta</td>
                <td>Hj. Tri Sulistyowati</td>
                <td>Univ. Trisakti</td>
              </tr>
              <tr>
                <td>13</td>
                <td>Banten</td>
                <td>Mirdedi</td>
                <td>Univ. Sultan Ageng Tirtayasa</td>
              </tr>
              <tr>
                <td>14</td>
                <td>Bengkulu</td>
                <td>Dr. Elektison Somi</td>
                <td>Univ. Bengkulu</td>
              </tr>
              <tr>
                <td>15</td>
                <td>Kalimantan Tengah</td>
                <td>Agus Mulyawan</td>
                <td>Univ. Palangkaraya</td>
              </tr>
              <tr>
                <td>16</td>
                <td>Sumatra Barat</td>
                <td>H. Ilhamdi Taufik</td>
                <td>Univ. Andalas</td>
              </tr>
              <tr>
                <td>17</td>
                <td>Lombok NTB</td>
                <td>Galang Asmara</td>
                <td>Univ. Mataram</td>
              </tr>
              <tr>
                <td>18</td>
                <td>NTT</td>
                <td>Ebu Cosmas</td>
                <td>Univ. Nusa Cendana</td>
              </tr>
              <tr>
                <td>19</td>
                <td>Jawa Tengah</td>
                <td>Sunarno Danusastro</td>
                <td>Univ. Sebelas Maret Surakarta</td>
              </tr>
              <tr>
                <td>20</td>
                <td>Sumatra Selatan</td>
                <td>Zulhidayat</td>
                <td>Univ. Sriwijaya</td>
              </tr>
              <tr>
                <td>21</td>
                <td>Sumatra Utara</td>
                <td>Armansyah</td>
                <td>Univ. Sumatera Utara (USU)</td>
              </tr>
              <tr>
                <td>22</td>
                <td>SulSel, Barat dan Tenggara</td>
                <td>Askari Razak</td>
                <td>Univ. Muslim Indonesia</td>
              </tr>
              <tr>
                <td>23</td>
                <td>Gorontalo</td>
                <td>Fence F Wantu</td>
                <td>Univ. Gorontalo</td>
              </tr>
            </table>
            <h5>Daftar Anggota & No KTA</h5>
            Unduh <a href="{{ url('unduh/7') }}">disini</a>.
          </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<style type="text/css">
  .card-body {
    background-color: #fff;
    color: #000;
  }
</style>
@endsection