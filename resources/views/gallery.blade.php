@extends('layouts.welcome')

@section('page_title', 'Unduhan')
@section('style')
<link rel="stylesheet" href="{{ url('vendor/blueimp/css/blueimp-gallery.min.css') }}">
@endsection
@section('script')
<script src="{{ url('vendor/blueimp/js/blueimp-gallery.min.js') }}"></script>
<script>
document.getElementById('links').onclick = function (event) {
    event = event || window.event;
    var target = event.target || event.srcElement,
        link = target.src ? target.parentNode : target,
        options = {index: link, event: event},
        links = this.getElementsByTagName('a');

    blueimp.Gallery(links, options);
    blueimp.Gallery(
        document.getElementById('links').getElementsByTagName('a'),
        {
            stretchImages: true
        }
    );
};
</script>
@endsection

@section('content')
<div class="container">
    <div class="row" style="margin-top: 20px">
        <div class="card bg-default mb-3" style="">
          <div class="card-header">
            <strong>Galeri</strong>
          </div>
          <div class="card-body">
            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
            <div id="blueimp-gallery" class="blueimp-gallery">
                <div class="slides"></div>
                <h3 class="title"></h3>
                <a class="prev">‹</a>
                <a class="next">›</a>
                <a class="close">×</a>
                <a class="play-pause"></a>
                <ol class="indicator"></ol>
            </div>
            <div class="row text-center text-lg-left">
              <div id="links">
                @foreach($albums as $album)
                <div class="col-lg-3 col-md-4 col-xs-6" style="float: left">
                  <a href="{{ url('img/'.$album->path) }}" class="d-block mb-4 h-100">
                      <img src="{{ url('img/'.$album->path) }}" class="img-fluid img-thumbnail">
                  </a>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection