@extends('layouts.welcome')

@section('page_title', 'Jurnal')

@section('content')
<div class="container">
    <div class="row" style="margin-top: 20px">
        <div class="card bg-default mb-3" style="">
          <div class="card-header">
            <strong>Jurnal</strong>
          </div>
          <div class="list-group">
            @foreach($journals as $journal)
            <a href="{{ url('jurnal/'.$journal->id) }}" class="list-group-item list-group-item-action flex-column align-items-start">
              <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">{{ $journal->name }}</h5>
                <small>3 days ago</small>
              </div>
              <p class="mb-1">{{ $journal->by }}</p>
              <small></small>
            </a>
            @endforeach
          </div>
          <!--ul class="list-group list-group-flush text-dark">
            @foreach($journals as $journal)
            <li class="list-group-item"><a href="{{ url('jurnal/'.$journal->id) }}">{{ $journal->name }}</a>
              <span>Baca Online</span></li>
            @endforeach
          </ul-->
        </div>
    </div>
</div>
@endsection