@extends('layouts.welcome')

@section('page_title', 'Jurnal')

@section('content')
<div class="container">
    <div class="card" style="margin-top: 20px">
  <div class="card-body">
    <a href="{{ url('jurnal') }}" class="btn btn-danger" style="margin-bottom: 10px">Kembali</a>
    <div id="example1"></div>
  </div>
</div>
</div>
@endsection

@section('style')
<style>
.pdfobject-container { height: 500px;}
.pdfobject { border: 1px solid #666; }
</style>
@endsection

@section('script')
<script src="{{ url('js/pdfobject.js') }}"></script>
<script>PDFObject.embed("{{ url('pdf/jurnal_penegasan_dan_penguatan_sistem_presidensiil.pdf') }}", "#example1");</script>
@endsection