<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::get('mars', function() {
	return view('mars');
});
Route::get('unduh', 'DownloadController@index');
Route::get('unduh/{id}', 'DownloadController@get');

Route::get('jurnal', 'JournalController@index');
Route::get('jurnal/{id}', 'JournalController@get');

Route::get('gallery', function() {
	$albums = DB::table('albums')->get();

	return view('gallery', ['albums' => $albums]);
});
Route::get('pengurus-anggota', function() {
	return view('pengurus-anggota');
});